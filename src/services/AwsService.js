import { CognitoIdentityClient } from "@aws-sdk/client-cognito-identity";
import { fromCognitoIdentityPool } from "@aws-sdk/credential-provider-cognito-identity";
import {
  DeleteObjectCommand,
  DeleteObjectsCommand,
  GetObjectCommand,
  ListObjectsCommand,
  PutObjectCommand,
  S3Client,
} from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";

const REGION = "ap-northeast-1"; //REGION
const albumBucketName = process.env.REACT_APP_ALBUM_BUCKET_NAME; //BUCKET_NAME
const identityPoolId = "ap-northeast-1:96d6100f-11b1-4e5f-aa97-94ac7c1d990c"; // IDENTITY_POOL_ID

// 创建s3客户端
const s3 = new S3Client({
  region: REGION,
  credentials: fromCognitoIdentityPool({
    client: new CognitoIdentityClient({ region: REGION }),
    identityPoolId: identityPoolId, // IDENTITY_POOL_ID
  }),
});

//列出bucket下所有的文件夹(相册)
const listAlbums = async () => {
  const data = await s3.send(
    new ListObjectsCommand({ Delimiter: "/", Bucket: albumBucketName })
  );
  if (data.CommonPrefixes) {
    return data.CommonPrefixes.map(function (commonPrefix) {
      const prefix = commonPrefix.Prefix;
      return decodeURIComponent(prefix.replace("/", ""));
    });
  } else {
    return [];
  }
};

// 在bucket中创建一个文件夹(相册)
const createAlbum = async (albumName) => {
  albumName = albumName.trim();
  const albumKey = encodeURIComponent(albumName);
  const key = albumKey + "/";
  await s3.send(new PutObjectCommand({ Bucket: albumBucketName, Key: key }));
};

// 获取文件夹(相册)中的内容(图片)
const viewAlbum = async (albumName) => {
  const albumPhotosKey = encodeURIComponent(albumName) + "/";
  const data = await s3.send(
    new ListObjectsCommand({
      Prefix: albumPhotosKey,
      Bucket: albumBucketName,
    })
  );

  return await Promise.all(
    data.Contents.filter((photo) => {
      return photo.Key.replace(albumPhotosKey, "") !== "";
    }).map((photo) => {
      return (async () => {
        const params = { Bucket: albumBucketName, Key: photo.Key };
        let getObjectCommand = new GetObjectCommand(params);
        return {
          // 获取对象的preSignedUrl来显示图片
          url: await getSignedUrl(s3, getObjectCommand, { expiresIn: 3600 }),
          name: photo.Key.replace(/^.*(\\|\/|\:)/, ""),
        };
      })();
    })
  );
};

// 向文件夹(相册)中上传图片
const addPhoto = async (albumName, file) => {
  const albumPhotosKey = encodeURIComponent(albumName) + "/";
  const fileName = file.name;
  const photoKey = albumPhotosKey + fileName;
  const uploadParams = {
    Bucket: albumBucketName,
    Key: photoKey,
    Body: file,
  };
  await s3.send(new PutObjectCommand(uploadParams));
};

// 删除文件夹(相册)中的一张图片
const deletePhotoFromS3 = async (albumName, photoName) => {
  const photoKey = encodeURIComponent(albumName) + "/" + photoName;
  const params = { Key: photoKey, Bucket: albumBucketName };
  await s3.send(new DeleteObjectCommand(params));
};

// 删除文件夹(相册)
const deleteAlbumFromS3 = async (albumName) => {
  const albumKey = encodeURIComponent(albumName) + "/";

  // 通过文件夹名称前缀获取要删除的对象
  const listParams = { Bucket: albumBucketName, Prefix: albumKey };
  const data = await s3.send(new ListObjectsCommand(listParams));
  const objects = data.Contents.map(function (object) {
    return { Key: object.Key };
  });

  // 执行删除
  const deleteParams = {
    Bucket: albumBucketName,
    Delete: { Objects: objects },
    Quiet: true,
  };
  await s3.send(new DeleteObjectsCommand(deleteParams));
};

export {
  listAlbums,
  createAlbum,
  viewAlbum,
  addPhoto,
  deletePhotoFromS3,
  deleteAlbumFromS3,
};
