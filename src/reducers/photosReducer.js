import { createSlice } from "@reduxjs/toolkit";
import { call, put, takeEvery } from "redux-saga/effects";
import { addPhoto, deletePhotoFromS3, viewAlbum } from "../services/AwsService";
import { toast } from "material-react-toastify";
import { START_LOADING, STOP_LOADING } from "./loadingReducer";

export const photosSlice = createSlice({
  name: "photos",
  initialState: {
    list: [],
  },
  reducers: {
    CLEAR_PHOTOS: (state) => {
      state.list = [];
    },
    PHOTOS_FETCH_SUCCEEDED: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { CLEAR_PHOTOS, PHOTOS_FETCH_SUCCEEDED } = photosSlice.actions;

function* fetchPhotos(action) {
  try {
    yield put(START_LOADING());
    const photos = yield call(viewAlbum, action.payload);
    yield put(PHOTOS_FETCH_SUCCEEDED(photos));
  } catch (error) {
    console.error(error);
    toast.error("获取照片失败");
  } finally {
    yield put(STOP_LOADING());
  }
}

function* uploadPhoto(action) {
  try {
    yield put(START_LOADING());
    const { albumName, file } = action.payload;
    yield call(addPhoto, albumName, file);
    yield call(fetchPhotos, { payload: albumName });
  } catch (error) {
    console.error(error);
    toast.error("上传照片失败");
  } finally {
    yield put(STOP_LOADING());
  }
}

function* deletePhoto(action) {
  try {
    yield put(START_LOADING());
    const { albumName, photoName } = action.payload;
    yield call(deletePhotoFromS3, albumName, photoName);
    yield call(fetchPhotos, { payload: albumName });
  } catch (error) {
    console.error(error);
    toast.error("删除照片失败");
  } finally {
    yield put(STOP_LOADING());
  }
}

function* watchPhotos() {
  yield takeEvery("FETCH_PHOTOS", fetchPhotos);
  yield takeEvery("UPLOAD_PHOTO", uploadPhoto);
  yield takeEvery("DELETE_PHOTO", deletePhoto);
}

export default photosSlice.reducer;

export { watchPhotos };
