import { combineReducers } from "redux";
import albumsReducer from "./albumsReducer";
import photosReducer from "./photosReducer";
import loadingReducer from "./loadingReducer";

const rootReducer = combineReducers({
  ...{
    albums: albumsReducer,
    photos: photosReducer,
    loading: loadingReducer,
  },
});

export default rootReducer;
