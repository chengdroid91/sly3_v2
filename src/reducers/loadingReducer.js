import { createSlice } from "@reduxjs/toolkit";

export const loadingSlice = createSlice({
  name: "loading",
  initialState: {
    global: false,
  },
  reducers: {
    START_LOADING: (state) => {
      state.global = true;
    },
    STOP_LOADING: (state) => {
      state.global = false;
    },
  },
});

export default loadingSlice.reducer;

export const { START_LOADING, STOP_LOADING } = loadingSlice.actions;
