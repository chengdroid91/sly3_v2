import { createSlice } from "@reduxjs/toolkit";
import { put, takeEvery, call } from "redux-saga/effects";
import {
  createAlbum,
  deleteAlbumFromS3,
  listAlbums,
} from "../services/AwsService";
import { toast } from "material-react-toastify";
import { START_LOADING, STOP_LOADING } from "./loadingReducer";

export const albumsSlice = createSlice({
  name: "albums",
  initialState: {
    list: [],
  },
  reducers: {
    ALBUMS_FETCH_SUCCEEDED: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { ALBUMS_FETCH_SUCCEEDED } = albumsSlice.actions;

function* fetchAlbums(action) {
  try {
    yield put(START_LOADING());
    const albums = yield call(listAlbums);
    yield put(ALBUMS_FETCH_SUCCEEDED(albums));
  } catch (error) {
    console.error(error);
    toast.error("获取相册失败");
  } finally {
    yield put(STOP_LOADING());
  }
}

function* addAlbum(action) {
  try {
    yield put(START_LOADING());
    yield call(createAlbum, action.payload);
    yield call(fetchAlbums);
  } catch (error) {
    console.error(error);
    toast.error("创建相册失败");
  } finally {
    yield put(STOP_LOADING());
  }
}

function* deleteAlbum(action) {
  try {
    yield put(START_LOADING());
    yield call(deleteAlbumFromS3, action.payload);
    yield call(fetchAlbums);
  } catch (error) {
    console.error(error);
    toast.error("删除相册失败");
  } finally {
    yield put(STOP_LOADING());
  }
}

function* watchAlbums() {
  yield takeEvery("ADD_ALBUM", addAlbum);
  yield takeEvery("DELETE_ALBUM", deleteAlbum);
  yield takeEvery("FETCH_ALBUMS", fetchAlbums);
}

export default albumsSlice.reducer;

export { watchAlbums };
