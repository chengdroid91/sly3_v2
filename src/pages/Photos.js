import { useDispatch, useSelector } from "react-redux";
import * as React from "react";
import {
  AppBar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  ImageList,
  ImageListItem,
  ImageListItemBar,
  Toolbar,
  Typography,
} from "@mui/material";
import { ArrowBackIcon, DeleteIcon } from "../icons";
import { FloatAddButton } from "../compoments/FloatAddButton";
import { useNavigate, useParams } from "react-router-dom";
import { useCallback, useEffect, useState } from "react";
import { CLEAR_PHOTOS } from "../reducers/photosReducer";

export function Photos() {
  const { albumName } = useParams();
  const [newPhotoFile, setNewPhotoFile] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const photoList = useSelector((state) => state?.photos?.list);

  const [openUploadPhotoDialog, setOpenUploadPhotoDialog] =
    React.useState(false);

  const onAddPhotoClick = useCallback(() => {
    setOpenUploadPhotoDialog(true);
  }, []);

  const handleAddPhotoDialogClose = useCallback(() => {
    setOpenUploadPhotoDialog(false);
    setNewPhotoFile(null);
  }, [setOpenUploadPhotoDialog, setNewPhotoFile]);

  const onPhotoFileChange = useCallback(
    (e) => {
      setNewPhotoFile(e.target.files);
    },
    [setNewPhotoFile]
  );

  const uploadPhoto = useCallback(() => {
    setOpenUploadPhotoDialog(false);
    if (newPhotoFile && newPhotoFile[0]) {
      dispatch({
        type: "UPLOAD_PHOTO",
        payload: {
          albumName: albumName,
          file: newPhotoFile[0],
        },
      });
    }
    setNewPhotoFile(null);
  }, [setOpenUploadPhotoDialog, dispatch, newPhotoFile, albumName]);

  useEffect(() => {
    return () => {
      dispatch(CLEAR_PHOTOS());
    };
  }, [dispatch]);

  useEffect(() => {
    dispatch({ type: "FETCH_PHOTOS", payload: albumName });
  }, [dispatch, albumName]);

  return (
    <Box>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="back"
            sx={{ mr: 2 }}
            onClick={() => {
              navigate(-1);
            }}
          >
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            {albumName}
          </Typography>
        </Toolbar>
      </AppBar>
      <ImageList cols={1} gap={8}>
        {photoList.map((item) => (
          <ImageListItem key={item.name}>
            <img
              src={`${item.url}`}
              srcSet={`${item.url}`}
              alt={item.name}
              loading="lazy"
            />
            <ImageListItemBar
              sx={{ height: "50px" }}
              title={item.name}
              actionIcon={
                <IconButton
                  sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                  onClick={(e) => {
                    e.stopPropagation();
                    dispatch({
                      type: "DELETE_PHOTO",
                      payload: {
                        albumName: albumName,
                        photoName: item.name,
                      },
                    });
                  }}
                >
                  <DeleteIcon />
                </IconButton>
              }
            />
          </ImageListItem>
        ))}
      </ImageList>
      <FloatAddButton onClick={onAddPhotoClick} />
      <Dialog
        open={openUploadPhotoDialog}
        fullWidth
        onClose={handleAddPhotoDialogClose}
      >
        <DialogTitle>请上传照片</DialogTitle>
        <DialogContent>
          <input
            id="photo-upload"
            accept="image/*"
            type="file"
            onChange={onPhotoFileChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAddPhotoDialogClose}>取消</Button>
          <Button onClick={uploadPhoto}>确定</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
