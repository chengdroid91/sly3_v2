import { useDispatch, useSelector } from "react-redux";
import {
  AppBar,
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { DeleteIcon, FolderIcon } from "../icons";
import * as React from "react";
import { FloatAddButton } from "../compoments/FloatAddButton";
import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function getItemList(albumsList, dispatch, navigate) {
  return albumsList.map((album) => {
    return (
      <ListItem
        onClick={() => {
          navigate(`/albums/${album}/photos`);
        }}
        key={album}
        secondaryAction={
          <IconButton
            edge="end"
            aria-label="delete"
            onClick={(e) => {
              e.stopPropagation();
              dispatch({ type: "DELETE_ALBUM", payload: album });
            }}
          >
            <DeleteIcon />
          </IconButton>
        }
      >
        <ListItemAvatar>
          <Avatar>
            <FolderIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={album} />
      </ListItem>
    );
  });
}

export function Albums() {
  const albumsList = useSelector((state) => state?.albums?.list);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [openAddAlbumDialog, setOpenAddAlbumDialog] = React.useState(false);
  const [newAlbumName, setNewAlbumName] = useState("");
  const [inputError, setInputError] = React.useState(false);
  const [helperMessage, setHelperMessage] = React.useState("");

  useEffect(() => {
    dispatch({ type: "FETCH_ALBUMS" });
  }, [dispatch]);

  const onAddAlbumClick = useCallback(() => {
    setOpenAddAlbumDialog(true);
  }, []);

  const closeAndClearDialogState = useCallback(() => {
    setNewAlbumName("");
    setHelperMessage("");
    setInputError(false);
    setOpenAddAlbumDialog(false);
  }, [setNewAlbumName, setInputError, setOpenAddAlbumDialog, setHelperMessage]);

  const handleAddAlbumDialogClose = useCallback(() => {
    closeAndClearDialogState();
  }, [closeAndClearDialogState]);

  const onAlbumNameChange = useCallback(
    (e) => {
      setNewAlbumName(e.target.value);
    },
    [setNewAlbumName]
  );

  const addAlbum = useCallback(() => {
    if (newAlbumName && newAlbumName.trim() !== "") {
      dispatch({ type: "ADD_ALBUM", payload: newAlbumName });
      closeAndClearDialogState();
    } else {
      setHelperMessage("请输入正确的相册名");
      setInputError(true);
    }
  }, [
    closeAndClearDialogState,
    dispatch,
    setInputError,
    newAlbumName,
    setHelperMessage,
  ]);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            我的相册
          </Typography>
        </Toolbar>
      </AppBar>
      <List dense>{getItemList(albumsList, dispatch, navigate)}</List>
      <FloatAddButton onClick={onAddAlbumClick} />
      <Dialog
        open={openAddAlbumDialog}
        fullWidth
        onClose={handleAddAlbumDialogClose}
      >
        <DialogTitle>请输入相册名</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            required
            error={inputError}
            margin="dense"
            id="albumName"
            label="相册名"
            type="text"
            fullWidth
            variant="standard"
            helperText={helperMessage}
            onChange={onAlbumNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAddAlbumDialogClose}>取消</Button>
          <Button onClick={addAlbum}>确定</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
