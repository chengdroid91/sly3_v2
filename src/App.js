import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { Albums } from "./pages/Albums";
import { Photos } from "./pages/Photos";
import { Backdrop, CircularProgress } from "@mui/material";
import { useSelector } from "react-redux";

function App() {
  const loading = useSelector((state) => {
    return !!state?.loading?.global;
  });

  return (
    <>
      <Routes>
        <Route path="/albums" element={<Albums />} />
        <Route path="/albums/:albumName/photos" element={<Photos />} />
        <Route path="*" element={<Navigate replace to="/albums" />} />
      </Routes>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
}

export default App;
