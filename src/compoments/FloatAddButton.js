import { Fab } from "@mui/material";
import { AddIcon } from "../icons";
import * as React from "react";

export function FloatAddButton({ onClick }) {
  return (
    <Fab
      onClick={onClick}
      color="primary"
      aria-label="add"
      sx={{
        right: 20,
        bottom: 20,
        position: "fixed",
      }}
    >
      <AddIcon />
    </Fab>
  );
}
