# 云基础第三节课的 WEB 小程序

## 部署到s3上的手顺

### 克隆代码并进入到代码目录
```
git clone https://gitlab.com/chengdroid91/sly3_v2.git
cd sly3_v2
```
### 配置环境变量
```
# 托管静态网站用的桶名
echo 'export STATIC_BUCKET_NAME="ilearn-albums-static-人名拼音"' >>~/.bash_profile

# 存放图片用的桶名
echo 'export ALBUM_BUCKET_NAME="ilearn-albums-人名拼音"' >>~/.bash_profile
echo 'export REACT_APP_ALBUM_BUCKET_NAME=${ALBUM_BUCKET_NAME}' >>~/.bash_profile

# 环境变量有效化
source ~/.bash_profile
```

### 存放图片用的桶的作成和配置
```
# 桶作成
aws s3 mb s3://${ALBUM_BUCKET_NAME} --region ap-northeast-1

# 配置cors策略
aws s3api put-bucket-cors --bucket ${ALBUM_BUCKET_NAME} \
      --cors-configuration file://cors.json

# 配置访问权限
aws s3api put-public-access-block \
    --bucket ${ALBUM_BUCKET_NAME} \
    --public-access-block-configuration "BlockPublicAcls=false,IgnorePublicAcls=false,BlockPublicPolicy=true,RestrictPublicBuckets=true"
```

### 托管静态网站用的桶的作成和配置
```
# 桶作成
aws s3 mb s3://${STATIC_BUCKET_NAME} --region ap-northeast-1

# 配置默认画面
aws s3 website s3://${STATIC_BUCKET_NAME}/ --index-document index.html --error-document index.html

# 配置访问权限
sed -i "s/{STATIC_BUCKET_NAME}/${STATIC_BUCKET_NAME}/" policy.json
aws s3api put-bucket-policy --bucket ${STATIC_BUCKET_NAME} --policy file://policy.json
```

### 编译前端代码
```
npm install
npm run build
```

### 将编译后的静态资源上传到托管静态网站用的桶中
```
aws s3 cp ./build/ s3://${STATIC_BUCKET_NAME}/  --recursive
```

### 通过下面的地址在浏览器上访问部署好的静态网站

http://ilearn-albums-static-人名拼音.s3-website-ap-northeast-1.amazonaws.com

### 截图请保留表示图片的画面

## License

The project is [MIT licensed](./LICENSE).
